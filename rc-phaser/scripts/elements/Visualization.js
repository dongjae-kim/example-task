function drawCity(scene, mmap, colorMajor, colorMinor){
    // define style
    var graphics = scene.add.graphics();
    graphics.fillStyle(colorMajor, 0.5);

    // draw cities except for the starting city
    for (var i = 1; i < mmap.xy.length; i++) {
        graphics.fillCircle(mmap.xy[i][0], mmap.xy[i][1], mmap.radius);
    };

    // draw the starting city
    graphics.fillStyle(colorMinor, 0.5);
    graphics.fillCircle(mmap.cityStart[0], mmap.cityStart[1], mmap.radius);
}

function drawBudget(scene, mmap, color, pointer) {
    // define style
    scene.budgetGraphics = scene.add.graphics();
    scene.budgetGraphics.lineStyle(4, color, 1.0);

    //budget follow mouse
    let x = mmap.choiceLocDyn[mmap.choiceLocDyn.length - 1][0];
    let y = mmap.choiceLocDyn[mmap.choiceLocDyn.length - 1][1];

    let radians = Math.atan2(pointer.y - y, pointer.x - x);

    var budgetPosX = x + mmap.budgetDyn[mmap.budgetDyn.length - 1] * Math.cos(radians);
    var budgetPosY = y + mmap.budgetDyn[mmap.budgetDyn.length - 1] * Math.sin(radians);

    //draw budget line
    scene.line = new Phaser.Geom.Line();
    scene.line.setTo(x, y, budgetPosX, budgetPosY);
    scene.budgetGraphics.strokeLineShape(scene.line);
  }

function drawRoad(scene, mmap, color){
    // define style
    scene.roadGraphics = scene.add.graphics();
    scene.roadGraphics.lineStyle(4, color, 1.0);

    for (var i = 0; i < mmap.choiceLocDyn.length-1; i++) {
        scene.road = new Phaser.Geom.Line(
        mmap.choiceLocDyn[i][0],mmap.choiceLocDyn[i][1],
        mmap.choiceLocDyn[i+1][0],mmap.choiceLocDyn[i+1][1]);
        scene.roadGraphics.strokeLineShape(scene.road);
    };
}

class scorebar {

    constructor(scene,mmap,color){
        //score bar parameters
        this.barWidth = 100;
        this.barHeight = 400;
        this.nrBox = 12;
        this.distToTop = 150; //distance to screen top
    
        this.boxCenter(scene); //center for labels
        this.incentive(); //calculate incentive: N^2
        this.drawScorebar(scene, color);
        this.indicator(scene,mmap,color);
        }
    
    boxCenter(scene){
        this.boxHeight = this.barHeight / this.nrBox;
        this.centerList = [];
        this.halfHeight = this.boxHeight / 2;
        this.x = this.barWidth / 2 + scene.cameras.main.width / 2 + 500;  //larger the number, further to right

        for (var i = 0; i < this.nrBox; i++){
            let boxLoc = [this.x, i * this.boxHeight + this.halfHeight];
            this.centerList.push(boxLoc);
        };
    }
  
    incentive(){
        this.Score = [];
        for (var i = 0; i < this.nrBox; i++){
            var newScore = Math.pow(i, 2);
            this.Score.push(newScore);
        };
    };

    //rendering score Bar
    drawScorebar(scene, color){
        //create rectangle and define style
        this.rect = scene.add.graphics();

        let colorBox = [0x66CC66,0x74C366,0x82B966,0x90B066,
                    0x9EA766,0xAC9E66,0xB99466,0xC78B66,
                    0xD58266,0xE37966,0xF16F66,0xFF6666] //color list

        for (var i = 0; i < this.nrBox; i++){

            let boxLoc = this.centerList[i];
            let text = this.Score[i];

            //score bar fill
            var r2 = scene.add.rectangle(boxLoc[0], boxLoc[1] + this.distToTop - this.halfHeight, this.barWidth, this.boxHeight, colorBox[i]);
            r2.setStrokeStyle(2, color);
            
            scene.add.text(boxLoc[0], boxLoc[1] + this.distToTop - this.halfHeight - 6, text, {fontFamily: 'Comic Sans MS', color:'#1C2833'});

        };

        // scorebar title
        scene.add.text(this.centerList[0][0]-50, this.centerList[0][1] + this.distToTop - 80,
                      'Bonus', {fontFamily: 'Comic Sans MS', fontSize: '26px', color:'#1C2833'});
    }

    indicator(scene,mmap,color){
        this.indicatorLoc = this.centerList[mmap.cityNr[mmap.cityNr.length-1]];

        //create triangle arrow and define style
        this.triangle = scene.add.graphics();
        this.triangle.fillStyle(color);

        //arrow parameter
        let point = [this.indicatorLoc[0] - 50, this.indicatorLoc[1] + this.distToTop - this.halfHeight];
        let v2 = [point[0] - 10, point[1] + 10];
        let v3 = [point[0] - 10, point[1] - 10];
        this.triangle.fillTriangle(point[0], point[1], v2[0], v2[1], v3[0], v3[1]);
        }

    whiteIndicator(scene,mmap,color){
        scene.max = mmap.cityNr.reduce(function(a, b) {
            return Math.max(a, b);
        });
        this.indicatorLocBest = this.centerList[scene.max]; // get the best result 

        //create triangle arrow and define style
        this.whiteTriangle = scene.add.graphics();
        this.whiteTriangle.fillStyle(color);

        //arrow parameter
        let point = [this.indicatorLocBest[0] - 50, this.indicatorLocBest[1] + this.distToTop - this.halfHeight];
        let v2 = [point[0] - 10, point[1] + 10];
        let v3 = [point[0] - 10, point[1] - 10];
        this.whiteTriangle.fillTriangle(point[0], point[1], v2[0], v2[1], v3[0], v3[1]);
        }
}
  